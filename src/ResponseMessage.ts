import { HttpStatus } from '@nestjs/common'

export class ResponseMessageError extends Error {}

export interface IMessage {
  code: string
  message: string
  payload?: any
}

export interface IResponseMessage {
  code: string
  message: string
  statusCode: HttpStatus
}

export interface IResponseMessages {
  type: string
  messages: IResponseMessage[]
}

export class ResponseMessage {
  public static responseCodes = {}

  public name: string
  public code: string
  public statusCode: number
  public message: string
  public payload: any

  constructor (messageCode: string, payload?: any) {
    const responseMessage = ResponseMessage.responseCodes[messageCode]

    if (!responseMessage) {
      throw Error('Could not find responseMessage')
    }

    this.name = this.constructor.name
    this.statusCode = responseMessage.statusCode
    this.code = messageCode
    this.message = responseMessage.message
    this.payload = payload
  }

  public static addResponseMessages (responseMessages: IResponseMessages) {
    responseMessages.messages.reduce((codes, error) => {
      codes[`${responseMessages.type}:${error.code}`] = error

      return codes
    }, ResponseMessage.responseCodes)
  }

  public static reply (response, messageCode: string, payload?: any) {
    const message = new ResponseMessage(messageCode, payload)

    if (!message.statusCode) {
      throw Error('ResponseMessage.reply() expects message to supply the statusCode.')
    }

    return response
      .status(message.statusCode)
      .json(message.toJSON())
  }

  /**
   * Used by the client part which receives the message.
   *
   * If the message is not what is expected a ResponseMessageError error is thrown.
   *
   * http().map((payload) => ResponseMessage.receive('user:emailSuccess', payload))
   */
  public static receive<TPayload = any> (code: string, response: IMessage): TPayload {
    if (response.code === code) {
      return response.payload
    }

    throw new ResponseMessageError('Invalid Response.')
  }

  public static receiver<T = any> (code: string) {
    return (response): T => ResponseMessage.receive<T>(code, response)
  }

  public toJSON (): IMessage {
    return {
      code: this.code,
      message: this.message,
      payload: this.payload
    }
  }

  public toString (): string {
    return JSON.stringify(this.toJSON(), null, 2)
  }
}
