# Nestling messages

As a convention you can suffix your files with `.messages.ts` and keep the response messages close to where they are used.

**user.messages.ts**
```typescript
import { HttpStatus } from '@nestjs/common'
import { IResponseMessages } from './ResponseMessage'

export const userMessages: IResponseMessages = {
  type: 'user',
  messages: [
    {
      code: 'sendEmailSuccess',
      statusCode: HttpStatus.OK,
      message: 'Email was send succesfully.'
    }
    ...etc
  ]
}
```
In order to use the response messages they must be registered first:

**user.module.ts**
```typescript
ResponseMessage.addResponseMessages(userMessages)
```

The response messages can be used like this:

**user.controller.ts**
```typescript
import { ResponseMessage } from '@nestling/messages'

@Controller('user')
export class UserController {

  @Post('/lost_password')
  @Bind(Body())
  async lostPassword(
    body: LostPassword,
    @Response() response
  ) {
     ...
     
     return new ResponseMessage('user:sendEmailSuccess')
  }
```

## Client Side Receiving

In able to validate response messages, the same lib can be used on the client side:


```typescript

ResponseMessage.addResponseMessages(userMessages)

const receiveEmailSuccessMessage = ResponseMessage.receiver('user:sendEmailSuccess')

// will throw a ResponseMessageError if the response code does not match the expectation.
const payload = receiveEmailSuccessMessage(response)

// Direct usage receive()
ResponseMessage.receive('user:sendEmailSuccess', response)

```

